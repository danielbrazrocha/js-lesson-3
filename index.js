/*    Criar uma função que, dado um número indefinido de arrays, faça a soma de seus valores    
Ex: [1,2,3] [1,2,2] [1,1] => 13        [1,1] [2, 20] => 24 */

let example1 = [1,2,3]

function newArray(... numArray){
    let sum = 0
    for(num1 of numArray){
        for (num2 of num1){
            sum += num2
        }
        
    }
    return sum
}

console.log(newArray(example1));
console.log(newArray([1,2,3],[1,2,2],[1,1]));
    


//    Criar uma função que dado um número n e um array, retorne    um novo array com os valores do array anterior * n    Ex: (2, [1,3,6,10]) => [2,6,12,20]        (3, [7,9,11,-2]) => [21, 27, 33, -6] */

function multiplicationNewArr (num, arr){
    let newArr = arr.map (item => item * num);
    return newArr;
}

console.log(multiplicationNewArr(2, example1));
console.log(multiplicationNewArr(2,[1,2,2,7]));


//    Crie uma função que dado uma string A e um array de strings,    retorne um array novo com apenas as strings do array que são    compostas exclusivamente por caracteres da string A        Ex: ("ab", ["abc", "ba", "ab", "bb", "kb"]) => ["ba", "ab", "bb"]        ("pkt", ["pkt", "pp", "pata", "po", "kkkkkkk"]) => ["pkt", pp, kkkkkkk] */

function filteredArray (string, arrString){
    let newArr = arrString.filter ((item) => {
        let ok = true

        for (stringValue in item){
            if(!(string.includes(item[stringValue])) ){
                ok = false
            }
        }
        if(ok){
            return item
        }
    })
    return(newArr)
}



console.log(filteredArray("ab", ["abc", "ba", "ab", "bb", "kb"]))



/*    Criar uma função que dado n arrays, retorne um novo array que possua apenas os valores que existem em todos os n arrays    
Ex: [1, 2, 3] [3, 3, 7] [9, 111, 3] => [3]        [120, 120, 110, 2] [110, 2, 130] => [110, 2] */

//Não consegui terminar este exercício
function valueInAllArrays(...arrays) {
    let numToBeChecked = arrays[0];
    let resultArray = [];
    console.log(numToBeChecked);
    for (array of arrays) {
        let currArr = arrays[array];        
        for (num of arrays) {
            console.log(currArr)
            if (arrays[array].includes(num))
                resultArray.push(num)
        }
        console.log(resultArray)
        
    }
    
}




//console.log(valueInAllArrays([1,2,3],[3,3,7],[9,111,3]));
console.log(valueInAllArrays([1,2,3],[1,2,2],[1,1]));


/*    Crie uma função que dado n arrays, retorne apenas os que tenham a    soma de seus elementos par    
Ex: [1, 1, 3] [1, 2, 2, 2, 3] [2] => [1, 2, 2, 2, 3] [2]        [2,2,2,1] [3, 2, 1] => [3,2,1] */




function onlySumParArrays(...arrays) {
    let parArrays = [];
    for (array in arrays){
        let currArr = arrays[array];       
        let res = currArr.reduce((accum, curr) => accum + curr);
        if (res % 2 === 0){
            parArrays.push(currArr)
        } 
    } 
    return parArrays
}

console.log(onlySumParArrays([1, 1, 3],[1, 2, 2, 2, 3],[2]));
